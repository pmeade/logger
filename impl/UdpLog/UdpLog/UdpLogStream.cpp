#include <IOutput.h>
#include <string>
#include <winsock2.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <time.h>
#include <algorithm>
#include <Ws2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
class UdpLogStream : public Mountains::IOutput {

private:
	enum class State {
		Connected,
		Closed,
		Error
	};

	static const int BUFLEN = 2048;
	static const int PORT = 5309;

	LogLevel		m_logLevel;
	std::fstream	m_logFile;
	State			m_state;
	SOCKET			m_socket;
	char			m_messageBuffer[BUFLEN];
	struct sockaddr_in m_server;
	int				m_nFromLen;
	WSADATA			m_wsa;

	std::string &Prefix(LogLevel level) {
		static std::string warn("WARNING: ");
		static std::string err("ERROR: ");
		static std::string rem("REMARK: ");

		if (level == LogLevel::Error) return err;
		if (level == LogLevel::Warning) return warn;
		return rem;
	}

	void Connect() {

		//Initialise winsock
		if (WSAStartup(MAKEWORD(2, 2), &m_wsa) != 0)
		{
			m_state = State::Error;
			return;
		}

		//Create a socket
		if ((m_socket = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
		{
			m_state = State::Error;
			return;
		}

		//Prepare the sockaddr_in structure
		m_server.sin_family = AF_INET;
		m_server.sin_port = htons(PORT);
		inet_pton(AF_INET, "127.0.0.1", &m_server.sin_addr);
		m_nFromLen = sizeof(m_server);

		m_state = State::Connected;
	}

	void Close() {
		m_state = State::Closed;
		closesocket(m_socket);
		WSACleanup();
		m_logFile.close();
	}

public:

	UdpLogStream() : m_logLevel(LogLevel::Remark) {
		Connect();
	}

	void Log(LogLevel level, std::string content) {
		if (static_cast<int>(level) <= static_cast<int>(m_logLevel)) {
			char message[BUFLEN];
			message[0] = static_cast<char>(level);
			size_t dataLen = min(BUFLEN - 2, content.length());
			strncpy_s(&message[1], BUFLEN - 2, content.c_str(), dataLen);
			message[dataLen + 1] = 0;

			if (sendto(m_socket, message, static_cast<int>(dataLen + 2), 0, (struct sockaddr *) &m_server, m_nFromLen))
			{
				std::cout << "sendto failed: " << WSAGetLastError() << std::endl;
			}
		}
	}


	void Flush() override {
	}

	void Remark(std::string remark) override {
		Log(LogLevel::Remark, remark);
	}

	void Warn(std::string warning) override {
		Log(LogLevel::Warning, warning);
	}

	void Error(std::string error) override {
		Log(LogLevel::Error, error);
	}

	void SetLogLevel(LogLevel logLevel) {
		m_logLevel = logLevel;
	}
};

Mountains::IOutput &Mountains::OpenDebugStream() {
	static UdpLogStream log;
	return log;
}
