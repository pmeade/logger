#include <IOutput.h>
#include <string>
#include <winsock2.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <time.h>
#include <algorithm>
#include <Ws2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
class FileLogStream : public Mountains::IOutput {

private:

	LogLevel		m_logLevel;
	std::fstream	m_logFile;

	std::string &Prefix(LogLevel level) {
		static std::string warn("WARNING: ");
		static std::string err("ERROR: ");
		static std::string rem("REMARK: ");

		if (level == LogLevel::Error) return err;
		if (level == LogLevel::Warning) return warn;
		return rem;
	}

public:

	FileLogStream() : m_logLevel(LogLevel::Remark) {
		m_logFile.open("FileLog.txt", std::ios::out);
	}

	void Log(LogLevel level, std::string content) {
		if (static_cast<int>(level) <= static_cast<int>(m_logLevel)) {
			m_logFile << Prefix(level) << content << std::endl;
		}
	}


	void Flush() override {
	}

	void Remark(std::string remark) override {
		Log(LogLevel::Remark, remark);
	}

	void Warn(std::string warning) override {
		Log(LogLevel::Warning, warning);
	}

	void Error(std::string error) override {
		Log(LogLevel::Error, error);
	}

	void SetLogLevel(LogLevel logLevel) {
		m_logLevel = logLevel;
	}
};

Mountains::IOutput &Mountains::OpenDebugStream() {
	static FileLogStream log;
	return log;
}
