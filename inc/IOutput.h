#pragma once
#include <string>

namespace Mountains {
	// Something that writes text to some kind of output context
	// Operations: Log
	// Defines: LogLevel (Remark, Warning, Error)
	class IOutput {
	public:
		enum class LogLevel
		{
			None,
			Error,
			Warning,
			Remark
		};

		virtual void Log(LogLevel level, std::string content) = 0;
		virtual void Flush() = 0;

		virtual void Remark(std::string content) = 0;
		virtual void Error(std::string content) = 0;
		virtual void Warn(std::string content) = 0;

		virtual void SetLogLevel(LogLevel level) = 0;
	};

	extern IOutput &OpenDebugStream();
}